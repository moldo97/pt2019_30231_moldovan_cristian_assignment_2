import java.util.Comparator;

public class Task implements Comparable<Task>{
    private int arrivalTime;
    private int finishTime;
    private int processingTime;

    public Task(int arrivalTime, int processingTime) {
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }


    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    @Override
    public int compareTo(Task task) {
        int compareArrival=((Task) task).getArrivalTime();
        return this.arrivalTime-compareArrival;
    }

    @Override
    public String toString() {
        return "Task{" +
                "arrivalTime=" + arrivalTime +
                ", processingTime=" + processingTime +
                '}';
    }
}
