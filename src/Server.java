import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicInteger serverNr;
    private AtomicInteger currentTime = new AtomicInteger(0);


    public Server(int nrTasks,int serverNr){
        //initialize queue and wp
        waitingPeriod=new AtomicInteger();
        tasks=new ArrayBlockingQueue<Task>(nrTasks);
        this.serverNr=new AtomicInteger(serverNr);
    }

    public void addTask(Task newTask){
        //add task to queue
        try {
            tasks.put(newTask);
            System.out.println("Put task: " + newTask.getArrivalTime() + " " + newTask.getProcessingTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //increment the waitingPeriod;
        waitingPeriod.addAndGet(newTask.getProcessingTime());
    }

    @Override
    public void run() {
        while (true) {
            //take next task from queue
            //stop the thread for a time equal with the task's processing time
            //decrement the waitingPeriod;
            try {
                if (!tasks.isEmpty()) {
                    Task task = tasks.element();

                    while (task.getProcessingTime() > 0) {
                        task.setProcessingTime(task.getProcessingTime() - 1);
                        waitingPeriod.decrementAndGet();

                        //System.out.println(task.toString());

                        Thread.sleep(1000);
                    }

                    tasks.take();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Task[] getTasks(){
        Task[] allTasks=new Task[tasks.size()];
        allTasks=tasks.toArray(allTasks);

        return allTasks;
    }
}
