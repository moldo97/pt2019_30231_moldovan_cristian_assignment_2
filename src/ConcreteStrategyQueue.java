import java.util.List;

public class ConcreteStrategyQueue implements Strategy{

    @Override
    public void addTasks(List<Server> servers, Task t) {
        int minQueue=10000;
        int serverNr=0;
        for(int i=0;i<servers.size();i++){
            if(servers.get(i).getTasks().length<minQueue){
                minQueue=servers.get(i).getTasks().length;
                serverNr=i;
            }
        }
        System.out.println("Adaug task-ul: " + t.toString() + " la serverul: " + serverNr);
        servers.get(serverNr).addTask(t);
    }
}
