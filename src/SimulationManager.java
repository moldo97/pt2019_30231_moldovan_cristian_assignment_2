import java.util.*;

public class SimulationManager implements Runnable {
    public int timeLimit=100; //maximum proccesing time -read from UI
    public int maxProcessingTime = 10;
    public int minProcessingTime = 2;
    public int numberOfServer = 3;
    public int numberOfClients = 100;
    public SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_QUEUE;

    //entity responsible with queue management and client distribution
    private Scheduler scheduler;
    //frame for displaying simulation
    private SimulationFrame frame;
    //pool of tasks (client  shopping in the store)
    private List<Task> generatedTasks;


    public SimulationManager(){
        //initialize the scheduler
        this.scheduler=new Scheduler(numberOfServer,numberOfClients);
        // create and start numberOfServers threads

        // initialize selection strategy -> createStrategy
        this.scheduler.changeStrategy(selectionPolicy);
        //initialize frame to display simulation
        this.frame=new SimulationFrame();
        //generate numberOfClients using generateNRandomTasks()
        this.generatedTasks=new ArrayList<Task>();
        generateNRandomTasks();
        //store them to generatedTasks



    }

    private void generateNRandomTasks(){
        //generate N random tasks
        // -random processing time
        // minProcessingTime < processingTime < maxProcessingTime
        // -random arrivalTime
        //sort list with respect to arrivalTime
        for(int i=0;i<numberOfClients;i++){
            Random r=new Random();

            int processingTime=r.nextInt(maxProcessingTime-minProcessingTime)+minProcessingTime;
            int arrivalTime=r.nextInt(timeLimit);
            Task task=new Task(arrivalTime,processingTime);

            generatedTasks.add(task);
        }

        Collections.sort(generatedTasks);


        for(int i=0;i<generatedTasks.size();i++){
            System.out.println(generatedTasks.get(i).toString());
        }

    }

    @Override
    public void run() {
        int currentTime = 0;
        while(currentTime<timeLimit){
            System.out.println("CT: " + currentTime);
            //iterate generatedTasks list and pic tasks that have the
            // arrivalTime equal with the currentTime

            Iterator tasks=generatedTasks.iterator();
            while(tasks.hasNext()){
                Task task=(Task) tasks.next();
                if(task.getArrivalTime()==currentTime){
                    scheduler.dispatchTask(task);
                    System.out.println(task.toString());
                    tasks.remove();
                }
            }
            // -send task to queue by calling the dispatchTask method
            //from Scheduler
            // -delete client from list
            //update UI frame
            frame.setTime(currentTime);

            List<Server> servers=scheduler.getServers();
            for(int i=0;i<servers.size();i++){
                frame.displayData(servers.get(i).getTasks(),i);
            }

            currentTime++;
            //wait an interval of 1 second
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        SimulationManager gen = new SimulationManager();
        Thread t = new Thread(gen);
        t.start();

    }
}
