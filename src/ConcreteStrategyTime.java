import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addTasks(List<Server> servers, Task t) {
        int minTime=10000;
        int serverNr=0;
        for(int i=0;i<servers.size();i++){
            Task[] tasks=servers.get(i).getTasks();
            int totalTime=0;

            for(int j=0;j<tasks.length;j++){
                totalTime += tasks[j].getProcessingTime();
            }

            if(totalTime<minTime){
                minTime=totalTime;
                serverNr=i;
            }
        }
        System.out.println("Adaug task-ul: " + t.toString() + " la serverul: " + serverNr);
        servers.get(serverNr).addTask(t);
    }
}
