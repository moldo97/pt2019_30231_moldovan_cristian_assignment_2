import javax.swing.*;
import java.awt.*;

public class SimulationFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel panel;
    private int WIDTH = 800 , HEIGHT = 800;

    JTextArea text1=new JTextArea();
    JScrollPane server1=new JScrollPane(text1);

    JTextArea text2=new JTextArea();
    JScrollPane server2=new JScrollPane(text2);

    JTextArea text3=new JTextArea();
    JScrollPane server3=new JScrollPane(text3);

    JLabel serv1=new JLabel();
    JLabel serv2=new JLabel();
    JLabel serv3=new JLabel();

    JLabel timp= new JLabel("Current time is : ...");

    public SimulationFrame(){
        panel = new JPanel();
        panel.setLayout(null);

        server1.setBounds(5,100,250,600);
        server2.setBounds(265,100,250,600);
        server3.setBounds(525,100,250,600);

        serv1.setText("Server 1");
        serv1.setBounds(50,50,50,50);

        serv2.setText("Server 2");
        serv2.setBounds(300,50,50,50);

        serv3.setText("Server 3");
        serv3.setBounds(565,50,50,50);

        timp.setBounds(300,15,100,50);

        panel.add(server1);
        panel.add(server2);
        panel.add(server3);

        panel.add(serv1);
        panel.add(serv2);
        panel.add(serv3);

        panel.add(timp);

        this.setSize(WIDTH,HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(panel);
        this.setVisible(true);

    }

    public void displayData(Task[] tasks, int nrServer){
        //remove all previous elements from panel and revalidate panel
        // add a JScrollPane with a JList containing tasks
        // add a scrollPane to panel
        // repaint and revalidate panel

        JTextArea choose=new JTextArea();

        if(nrServer==0) {
            choose = text1;
        }
        else if (nrServer==1){
            choose=text2;
            }
        else if (nrServer==2){
            choose=text3;
        }

        choose.setText("");
        for(int i=0;i<tasks.length;i++) {
            choose.append("Task : " + String.valueOf(tasks[i].getArrivalTime()) + " cu timpul : " + String.valueOf(tasks[i].getProcessingTime()) + "\n");
            choose.repaint();
            choose.revalidate();
        }
    }

    public void setTime(int currentTime){
        timp.setText(String.valueOf(currentTime));
    }
}
