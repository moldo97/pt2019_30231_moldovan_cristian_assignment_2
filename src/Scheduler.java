import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers, int maxTasksPerServer){
        //for maxNoServers
        // - create server object
        // - create thread with t
        // for he object

        this.maxNoServers=maxNoServers;
        this.maxTasksPerServer=maxTasksPerServer;
        servers=new ArrayList<>();

        for(int i=0;i<this.maxNoServers;i++){
            Server server=new Server(this.maxTasksPerServer,i);
            servers.add(server);
            Thread t=new Thread(server);
            t.start();
        }
    }

    public void changeStrategy(SelectionPolicy policy){
        //apply strategy patter to instantiate the strategy with the concrete
        //strategy corresponding to policy

        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy=new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy=new ConcreteStrategyTime();
        }

    }

    public void dispatchTask(Task t){
        //call the strategy addTask method
        strategy.addTasks(servers,t);
    }

    public List<Server> getServers(){
        return servers;
    }
}
