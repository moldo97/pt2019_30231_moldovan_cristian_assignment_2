import java.util.List;

public interface Strategy {
    public void addTasks(List<Server> servers, Task t);
}
